import express from "express";
import fs, { mkdirSync, readdir, rm } from "fs";
import fetch from "node-fetch";
import unzipper from "unzipper";
import { exec } from "child_process";
const app = express();
const port = 3000;

app.get("/", (req, res) => {
  res.send("Vulkan Symbol Server");
});

const addFilesToStore = async (version: string) => {
  console.log("Adding files to storage...");
  const readdir = (path: string) => {
    const files: string[] = [];
    fs.readdirSync(path).forEach((file) => {
      const fullpath = path + "/" + file;
      if (fs.lstatSync(fullpath).isDirectory())
        readdir(fullpath).forEach((subdirFile) => files.push(subdirFile));
      else files.push(fullpath);
    });
    return files;
  };

  await Promise.all(
    readdir(`storage/tmp/${version}/`).map((filepath) => {
      if (filepath.endsWith(".pdb") || filepath.endsWith(".dll") || filepath.endsWith(".exe")) {
        const filename = filepath.substr(filepath.lastIndexOf("/") + 1);
        console.log(`${filepath}`);
        const symbolLookup = exec(`bin/SymbolPath ${filepath}`, (err, stdout, stderr) => {
          if (err) {
            console.log(`${stderr} ${stdout} ${err}`);
            return;
          }
          const id = stdout.trim();
          if (!fs.existsSync(`storage/${filename}`)) mkdirSync(`storage/${filename}`);
          if (!fs.existsSync(`storage/${filename}/${id}`)) mkdirSync(`storage/${filename}/${id}`);

          if (fs.existsSync(`storage/${filename}/${id}/${filename}`)) return;

          fs.renameSync(filepath, `storage/${filename}/${id}/${filename}`);
          return;
        });
        return new Promise((fulfill) => symbolLookup.on("close", fulfill));
      }
    })
  ).then(() => {
    try {
      fs.rmSync(`storage/tmp/${version}`, { recursive: true });
    } catch {}
  });
};

const extractRuntime = async (version: string) => {
  // path
  console.log(`Extracting runtime ${version}`);
  const zip = fs
    .createReadStream(`storage/tmp/${version}.zip`)
    .pipe(unzipper.Parse({ forceStream: true }));
  for await (const entry of zip) {
    console.log(`  entry: ${entry.path}`);
    if (entry.type === "Directory")
      fs.mkdirSync(`storage/tmp/${version}/${entry.path}`, { recursive: true });
    else {
      const dir = `storage/tmp/${version}/${entry.path.substr(0, entry.path.lastIndexOf("/"))}`;
      if (!fs.existsSync(dir)) fs.mkdirSync(dir);
      entry.pipe(fs.createWriteStream(`storage/tmp/${version}/${entry.path}`));
    }
  }
};

const downloadRuntime = async (version: string) => {
  if (!fs.existsSync("storage/tmp")) fs.mkdirSync("storage/tmp");
  if (!fs.existsSync(`storage/tmp/${version}`)) fs.mkdirSync(`storage/tmp/${version}`);

  console.log(`Downloading runtime ${version}`);
  if (!fs.existsSync(`storage/tmp/${version}.zip`)) {
    const archive = fs.createWriteStream(`storage/tmp/${version}.zip`, { autoClose: true });
    await fetch(
      `https://sdk.lunarg.com/sdk/download/${version}/windows/VulkanRT-${version}-Components.zip`
    )
      .then((r) => r.body.pipe(archive, { end: true }))
      .then((stream) => new Promise((fulfill) => stream.on("finish", fulfill)));
  }
  return extractRuntime(version).then(() => {
    addFilesToStore(version);

    // save text file that we're done with download so we don't have to download again
    const writer = fs.createWriteStream(`storage/${version}.txt`);
    writer.write(version);
    writer.close();
    try {
      rm(`storage/tmp/${version}.zip`, () => {});
    } catch {}
  });
};

const updateSymbolStore = async () => {
  if (!fs.existsSync("storage")) fs.mkdirSync("storage");

  await fetch("https://vulkan.lunarg.com/sdk/versions/windows.json")
    .then((res) => res.json())
    .then((versions: string[]) => {
      return Promise.all(
        versions.map((ver) => {
          if (!fs.existsSync(`storage/${ver}.txt`) && !ver.startsWith("1.1")) {
            // no runtime zips for 1.1.* versions
            return downloadRuntime(ver);
          }
        })
      );
    });
};

let lastUpdateTime: number = Date.now();

app.get("/:file/:id/:file2", async (req, res, next) => {
  if (req.params["file"] !== req.params["file2"]) {
    res.sendStatus(404);
    return;
  } else if (
    !req.params["file"].startsWith("vulkan-1.") &&
    !req.params["file"].startsWith("vulkaninfo.")
  ) {
    res.statusCode = 404;
    res.send("This server only hosts vulkan-1.exe/pdb and vulkaninfo.exe/pdb");
    return;
  }
  const filepath = `storage/${req.params["file"]}/${req.params["id"]}/${req.params["file"]}`;
  if (fs.existsSync(filepath)) {
    return res.download(filepath, function (err) {
      if (!err) return;
      return next(err);
    });
  }

  if (lastUpdateTime + 600 < Date.now()) {
    res.statusCode = 404;
    res.send(
      "Can't find that file, and not checking on LunarG server for newer files as we did that less then 10 minutes ago!"
    );
    return;
  }
  lastUpdateTime = Date.now();
  await updateSymbolStore().then(() => {
    res.download(filepath, function (err) {
      if (!err) return;
      res.statusCode = 404;
      res.send("Can't find that file, sorry!");
    });
  });
});

// update symbol store on startup first then whenever we hit a non existing 404 symbol we'll do it again in the get
// this way we make sure to download all known symbols on startup
updateSymbolStore().then(() => {
  lastUpdateTime = Date.now();
  app.listen(port, () => {
    console.log(`Vulkan Symbol Server listening at http://localhost:${port}`);
  });
});
