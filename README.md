# Vulkan SDK runtime symbol server

This Node.js app downloads and serves all runtimes from LunarG on Symbol Server paths as required for Visual Studio.

A public server is up and running on https://vulkansymbols.paralleldimension.nl/

This uses the [SDK API](https://vulkan.lunarg.com/content/view/latest-sdk-version-api) to download
new versions automatically when they are released and when a current symbol could not be found on the server.

Also uses [SymbolPath](https://github.com/sandercox/symbolpath/) to obtain path information for hosting the
symbols on a Symbol Server.

# Want to run your own server?

Build it using `docker` or use the docker container image as build using GitLab CI.
